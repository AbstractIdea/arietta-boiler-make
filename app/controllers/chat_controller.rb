class ChatController < WebsocketRails::BaseController
  def initialize_session
    # perform application setup here
    controller_store[:message_count] = 0
  end

  def index
    new_message = {:message => 'this is a message'}
    send_message :event_name, new_message
  end

  def awesomeness_approval
    if message[:awesomeness] > 5
      new_message = {:message => 'hello'}
      trigger_success new_message
    else
      new_message = {:message => 'world'}
      trigger_failure new_message
    end
  end
end
