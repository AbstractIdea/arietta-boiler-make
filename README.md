# README #
Project Arietta strives to be a collaborative music creator.  This repo is a final snapshot of the work produced by the Arietta team during Purdue's 2014 Boiler Make hackathon.

Given that this project was created at a hackathon, there is very little documentation.  This repo is mainly intended for teammates to later retrieve the project (and to use as a historical backup).

Happy Hacking,

Team Arietta - Marwan Nakhaleh, Ranvit Reddy, and Matthew Vaughn